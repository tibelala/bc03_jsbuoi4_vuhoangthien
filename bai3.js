var Dem = function () {
  var soThuNhatvalue = document.getElementById("txt_so_thu_1").value * 1;
  var soThuHaivalue = document.getElementById("txt_so_thu_2").value * 1;
  var soThuBavalue = document.getElementById("txt_so_thu_3").value * 1;
  var timSoThuNhat = soThuNhatvalue % 2 == 0;
  var timSoThuHai = soThuHaivalue % 2 == 0;
  var timSoThuBa = soThuBavalue % 2 == 0;
  var soChan = timSoThuNhat + timSoThuHai + timSoThuBa;
  var soLe = 3 - soChan;
  document.getElementById(
    "ket_qua"
  ).innerHTML = `Có ${soChan} số chẵn, ${soLe} số lẻ`;
};
