var duDoan = function () {
  var canhThuNhatvalue = document.getElementById("txt_canh_thu_1").value * 1;
  var canhThuHaivalue = document.getElementById("txt_canh_thu_2").value * 1;
  var canhThuBavalue = document.getElementById("txt_canh_thu_3").value * 1;
  var canhThuNhatx2 = Math.pow(canhThuNhatvalue, 2);
  var canhThuHaix2 = Math.pow(canhThuHaivalue, 2);
  var canhThubax2 = Math.pow(canhThuBavalue, 2);

  if (
    canhThuNhatvalue == canhThuHaivalue &&
    canhThuNhatvalue == canhThuBavalue
  ) {
    document.getElementById("ket_qua").innerHTML = `<p>Tam Giác Đều</p>`;
  } else if (
    (canhThuNhatvalue == canhThuHaivalue &&
      canhThuNhatvalue != canhThuBavalue) ||
    (canhThuNhatvalue == canhThuBavalue &&
      canhThuNhatvalue != canhThuHaivalue) ||
    (canhThuHaivalue == canhThuBavalue && canhThuHaivalue != canhThuNhatvalue)
  ) {
    document.getElementById("ket_qua").innerHTML = `<p>Tam Giác Cân</p>`;
  } else if (
    canhThuNhatx2 == canhThuHaix2 + canhThubax2 ||
    canhThuHaix2 == canhThuNhatx2 + canhThubax2 ||
    canhThubax2 == canhThuNhatx2 + canhThuHaix2
  ) {
    document.getElementById("ket_qua").innerHTML = `<p>Tam Giác Vuông</p>`;
  } else {
    document.getElementById(
      "ket_qua"
    ).innerHTML = `<p>Một loại tam giác nào đó</p>`;
  }
};
